import { __RewireAPI__ as PageLoadMiddlewareRewireAPI } from '../../src/router/middlewares/pageLoad';
import { __RewireAPI__ as CellsStaticRouterRewireAPI } from '../../src/router/index';

PageLoadMiddlewareRewireAPI.__set__('isCustomElementRegistered', function() {
  return true;
});

CellsStaticRouterRewireAPI.__set__('pageRender', (mainNode) => () => {
  return {
    onTransitionSuccess(toState, fromState) {

    }
  }
});

CellsStaticRouterRewireAPI.__set__('progressivePageRender', (maxSimultaneousPages) => (mainNode) => () => {
  return {
    onTransitionSuccess(toState, fromState) {

    }
  }
});
