export const buildHooks = (routerHooks, routeHooks) => {
  const hooks = [];

  if (routerHooks) {
    Array.prototype.push.apply(hooks, ...routerHooks);
  }

  if (routeHooks) {
    Array.prototype.push.apply(hooks, ...routeHooks);
  }

  return hooks;
}

export const getHooksFromRouter = (hooks, routerHooks) => hooks.map(hook => getHookFromRouter(hook, routerHooks));

export const getHookFromRouter = (hook, routerHooks) => extractProperty(hook, routerHooks);

const extractProperty = (key, value) => {
  if (!key || !value) {
    return null;
  }

  if (value.hasOwnProperty(key)) {
    return value[key];
  }

  return null;
};

export const getHooksFromRoutes = (definitions, routes) => definitions.map(definition => getHookFromRoute(definition.hook, definition.route, routes)) || [];

export const getHookFromRoute = (hook, state, routes) => {
  if (!state || !routes) {
    return null;
  }

  const target = routes.find(r => r.name === state.name);

  if (target) {
    return extractProperty(hook, target);
  }

  return null;
}

export const triggerHooks = (hooks, toState, fromState, err) => hooks.filter(Boolean).forEach(hook => hook.call(null, toState, fromState, err));

export default {
  buildHooks,
  getHooksFromRouter,
  getHookFromRouter,
  getHooksFromRoutes,
  getHookFromRoute,
  triggerHooks
};
