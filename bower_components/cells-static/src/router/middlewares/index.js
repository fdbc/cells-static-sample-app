import pageLoad from './pageLoad';
import routeResolve from './routeResolve';

export default {
  pageLoad,
  routeResolve
};
